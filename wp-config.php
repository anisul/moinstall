<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Heroku Postgres settings - from Heroku Environment ** //
$db = parse_url($_ENV["DATABASE_URL"]);

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', trim($db["path"],"/"));

/** MySQL database username */
define('DB_USER', $db["user"]);

/** MySQL database password */
define('DB_PASSWORD', $db["pass"]);

/** MySQL hostname */
define('DB_HOST', $db["host"]);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-%0IlW/+[dG-^~x$9?o*;S(VYas+Uik/F:2`J~!L3s:(6)P.+[6R|{-/hC+PS<sa');
define('SECURE_AUTH_KEY',  ',9VLgXe(R,1&>ha3~G-Cw|mo^[Ap4Wo0kRo@^]P(z -Uoe[NtWt(o!0#Cq*}D2Ul');
define('LOGGED_IN_KEY',    'A!s M,%abS|of]-iIVtRttIYCoIfy{MC%-SI+32c1x=U&iHBtIV&c&j}w6MSv7g>');
define('NONCE_KEY',        'rJ|x,~_I[|-Fb)2k0&(_(BDgPz0PX@mJomLP2)Bx,2?p605+pj(J{NCeD&77tRCy');
define('AUTH_SALT',        '{Aau&!=izUxIN_:xXD+)E0F=JBjtO]U1JiItE+u(=1QqT!Mb4uF{gnH<54Q:-5|r');
define('SECURE_AUTH_SALT', 'u]c~F@g!}X&AY+>Z~1VTfxpxMkn,&@S[q#dk:MI1gL1CPgs)8-]3bEoPn.y#.J^p');
define('LOGGED_IN_SALT',   '#d`]q`;R(0VpG?$idO1R,Ui(#xl=fUY%ngqJe K3pz5.cp1[D<ud@]y#OET*|D2)');
define('NONCE_SALT',       '( x1BU3*UXm3F372|l&0&{`#*8G2.Sj#X{NRb[zR;7g-%$S|ts<9Q%6#c|!7`rl8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

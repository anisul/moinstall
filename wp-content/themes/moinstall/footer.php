				<!-- BOTTOM PANEL : begin -->
				<div id="bottom-panel">

					<div class="container">
						<div class="row">
							<div class="col-md-4">

								<!-- EVENTS WIDGET : begin -->
								<div class="widget blog-widget">
									<h3 class="widget-title">EVENTS</h3>
									<div class="widget-content">

										<ul>
											<li>
												<h4><a href="#">MWC, Barcelona</a></h4>
												<p class="date">Mar 2-5, 2015</p>
											</li>
											<li>
												<h4><a href="#">GMIC, Beijing </a></h4>
												<p class="date">April 28-30, 2015</p>
											</li>
											<li>
												<h4><a href="#">Apps World San Francisco</a></h4>
												<p class="date">May 12-13, 2015</p>
											</li>
											<li>
												<h4><a href="#">Casual Connect San Francisco </a></h4>
												<p class="date">Aug 11-13, 2015</p>
											</li>
										</ul>

									</div>
								</div>
								<!-- EVENTS WIDGET : end -->

							</div>
							<div class="col-md-4">

								<!-- BLOG NEWS : begin -->
								<div class="widget blog-widget">
									<h3 class="widget-title">NEWS</h3>
									<div class="widget-content">

										<ul>
											<li>
												<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ventureBeat.png" alt="">
												<h4><a href="#">Appnext releases its self-serve mobile advertising platform with cost controls</a></h4>
											</li>
											<li>
												<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/insideMa.png" alt="">
												<h4><a href="#">Appnext launches self-serve ad platform with cpi bidding for app advertisers</a></h4>
											</li>
										</ul>

									</div>
								</div>
								<!-- NEWS WIDGET : end -->

							</div>
							<div class="col-md-4">

								<!-- BOLG WIDGET : begin -->
								<div class="widget blog-widget">
									<h3 class="widget-title">BLOG</h3>
									<div class="widget-content">

										<ul>
											<li>
												<h4><a href="#">7 Resources for A Winning App Marketing Strategy</a></h4>
											</li>
											<li>
												<h4><a href="#">Special Valentine’s Bonuses for The Appnext Publishers</a></h4>
											</li>
											<li>
												<h4><a href="#">The Appnext Partner Stories: UC Web</a></h4>
											</li>
										</ul>

									</div>
								</div>
								<!-- BLOG WIDGET : end -->

							</div>
						</div>
					</div>

					<!-- BACK TO TOP : begin -->
					<a href="#header" id="back-to-top" title="Back to top"><i class="fa fa-chevron-up"></i></a>
					<!-- BACK TO TOP : end -->

				</div>
				<!-- BOTTOM PANEL : end -->

			</div>
			<!-- WRAPPER : end -->			<!-- FOOTER : begin -->
			<div id="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-push-6">

							<!-- FOOTER MENU : begin -->
							<nav class="footer-menu">
								<ul>
									<li><a href="#">TERMS OF USE</a></li>
									<li><a href="#">PRIVACY POLICY</a></li>
									<li><a href="#">CONTACT</a></li>
								</ul>
							</nav>
							<!-- FOOTER MENU : end -->

						</div>
						<div class="col-md-6 col-md-pull-6">

							<!-- FOOTER TEXT : begin -->
							<div class="footer-text">
								<a href="#">MOINSTALL &copy; 2015</a>
							</div>
							<!-- FOOTER TEXT : end -->

						</div>
					</div>
				</div>
			</div>
			<!-- FOOTER : end -->

		</div>
		<!-- BODY : end -->
<?php wp_footer(); ?>
</body>
</html>
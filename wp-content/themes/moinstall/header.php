<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<link rel="icon" type="image/x-icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/files/favicon.ico" />
	<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;
	wp_title( '|', true, 'right' );
	// Add the blog name.
	bloginfo( 'name' );
	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	?></title>
	<!--[if lt IE 8]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5shiv.js" type="text/javascript"></script>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>
<body>
		<!-- HEADER : begin -->
		<header id="header" class="m-large m-enable-fixed m-fixed-bg">
			<div class="header-inner">
				<div class="container">

					<!-- HEADER CONTENT : begin -->
					<div class="header-content">

						<!-- HEADER BRANDING : begin -->
						<div class="header-branding">
							<div class="header-branding-inner">
								<a href="#"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.png" width="230" height="230" alt="Vibes" data-hires="images/logo-large.2x.png" data-fixed="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.png" data-fixed-hires="<?php echo esc_url( get_template_directory_uri() ); ?>//logo.png" data-fixed-width="52" data-fixed-height="52"></a>
							</div>
						</div>
						<!-- HEADER BRANDING : end -->

						<!-- HEADER MENU : begin -->
						<nav class="header-menu">
							<ul>
								<li class="m-active m-has-submenu m-active-default">
									<a href="#">Home</a>
								<li class="m-has-submenu">
									<a href="#">Platform</a>
								<button class="toggle" type="button"><i class="fa"></i></button></li>
								<li class="m-has-submenu">
									<a href="#">Monitize</a>
								<button class="toggle" type="button"><i class="fa"></i></button></li>
								<li class="m-has-submenu">
									<a href="#">Advertise</a>
								<button class="toggle" type="button"><i class="fa"></i></button></li>
								<li><a href="#">Community</a></li>
								<li><a href="#">About</a></li>
								<li><a href="#">Sign in</a></li>
								<li><a href="#">Join Now</a></li>
							</ul>
						</nav>
						<!-- HEADER MENU : end -->

					</div>
					<!-- HEADER CONTENT : end -->


				</div>
			</div>
		</header>
		<!-- HEADER : end -->
<?php wp_head(); ?>
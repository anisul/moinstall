							<section class="c-section">
								<div class="section-inner">

									<div class="c-gallery m-has-gallery-tools-top m-images-loaded"><div class="gallery-tools-top"><div class="container"></div></div>

										<!-- ITEM LIST : begin -->
										<ul class="item-list" data-layout="fitRows" style="position: relative; height: 460.875px;">

											<!-- ITEM : begin -->
											<li class="item m-has-overlay m-animated cat-flyers cat-print" style="position: absolute; left: 0px; top: 0px;">
												<a href="#" data-ajax-selector=".portfolio-detail" class="item-image m-open-ajax-modal-project"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/project_01.jpg" alt=""></a>
												<div class="item-info">
													<h4><a href="#" data-ajax-selector=".portfolio-detail" class="m-open-ajax-modal-project">With Animation</a></h4>
													<p>Far far away, behind the word mountains, far from the countries Vokalia.</p>
												</div>
												<div class="item-tools">
													<a href="#"><i class="fa fa-link"></i></a>
												</div>
											</li>
											<!-- ITEM : end -->

											<!-- ITEM : begin -->
											<li class="item m-has-overlay m-animated cat-animation" style="position: absolute; left: 307px; top: 0px;">
												<a href="#" data-ajax-selector=".portfolio-detail" class="item-image m-open-ajax-modal-project">
													<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/project_02.jpg" alt="">
													<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/project_09.jpg" alt="">
												</a>
												<div class="item-info">
													<h4><a href="#" data-ajax-selector=".portfolio-detail" class="m-open-ajax-modal-project">Second Image Animated</a></h4>
													<p>Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
												</div>
												<div class="item-tools">
													<a href="#"><i class="fa fa-link"></i></a>
												</div>
											</li>
											<!-- ITEM : end -->

											<!-- ITEM : begin -->
											<li class="item m-has-overlay cat-pixelart" style="position: absolute; left: 614px; top: 0px;">
												<a href="#" data-ajax-selector=".portfolio-detail" class="item-image m-open-ajax-modal-project">
													<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/project_03.jpg" alt="">
													<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/project_10.jpg" alt="">
												</a>
												<div class="item-info">
													<h4><a href="#" data-ajax-selector=".portfolio-detail" class="m-open-ajax-modal-project">Second Image</a></h4>
													<p>A small river named Duden flows by their place and supplies it with.</p>
												</div>
												<div class="item-tools">
													<a href="#"><i class="fa fa-link"></i></a>
												</div>
											</li>
											<!-- ITEM : end -->

											<!-- ITEM : begin -->
											<li class="item m-has-overlay cat-animation" style="position: absolute; left: 921px; top: 0px;">
												<a href="#" data-ajax-selector=".portfolio-detail" class="item-image m-open-ajax-modal-project"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/project_04.jpg" alt=""></a>
												<div class="item-info">
													<h4><a href="#" data-ajax-selector=".portfolio-detail" class="m-open-ajax-modal-project">No Animation</a></h4>
													<p>It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
												</div>
												<div class="item-tools">
													<a href="#"><i class="fa fa-link"></i></a>
												</div>
											</li>
											<!-- ITEM : end -->

											<!-- ITEM : begin -->
											<li class="item m-animated cat-flyers cat-pixelart" style="position: absolute; left: 0px; top: 230px;">
												<a href="#" data-ajax-selector=".portfolio-detail" class="item-image m-open-ajax-modal-project">
													<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/project_05.jpg" alt="">
													<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/project_11.jpg" alt="">
												</a>
												<div class="item-info">
													<h4><a href="#" data-ajax-selector=".portfolio-detail" class="m-open-ajax-modal-project">No Overlay</a></h4>
													<p>Even the all-powerful Pointing has no control about the blind texts it is an almost.</p>
												</div>
												<div class="item-tools">
													<a href="#"><i class="fa fa-link"></i></a>
												</div>
											</li>
											<!-- ITEM : end -->

											<!-- ITEM : begin -->
											<li class="item m-has-overlay cat-animation" style="position: absolute; left: 307px; top: 230px;">
												<a href="#" data-ajax-selector=".portfolio-detail" class="item-image m-open-ajax-modal-project"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/project_06.jpg" alt=""></a>
												<div class="item-info">
													<h4><a href="#" data-ajax-selector=".portfolio-detail" class="m-open-ajax-modal-project">No Animation</a></h4>
													<p>One day however a small line of blind text by the name of Lorem Ipsum decided.</p>
												</div>
												<div class="item-tools">
													<a href="#"><i class="fa fa-link"></i></a>
												</div>
											</li>
											<!-- ITEM : end -->

											<!-- ITEM : begin -->
											<li class="item m-has-overlay m-animated m-video cat-print" style="position: absolute; left: 614px; top: 230px;">
												<a href="#" data-ajax-selector=".portfolio-detail" class="item-image m-open-ajax-modal-project"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/project_07.jpg" alt=""></a>
												<div class="item-info">
													<h4><a href="#" data-ajax-selector=".portfolio-detail" class="m-open-ajax-modal-project">With Animation</a></h4>
													<p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas.</p>
												</div>
												<div class="item-tools">
													<a href="#"><i class="fa fa-link"></i></a>
												</div>
											</li>
											<!-- ITEM : end -->

											<!-- ITEM : begin -->
											<li class="item m-has-overlay cat-animation cat-pixelart" style="position: absolute; left: 921px; top: 230px;">
												<a href="#" data-ajax-selector=".portfolio-detail" class="item-image m-open-ajax-modal-project">
													<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/project_08.jpg" alt="">
													<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/project_12.jpg" alt="">
												</a>
												<div class="item-info">
													<h4><a href="#" data-ajax-selector=".portfolio-detail" class="m-open-ajax-modal-project">Second Image</a></h4>
													<p>Question Marks and devious Semikoli, but the Little Blind Text didn't listen.</p>
												</div>
												<div class="item-tools">
													<a href="#"><i class="fa fa-link"></i></a>
												</div>
											</li>
											<!-- ITEM : end -->

										</ul>
										<!-- ITEM LIST : end -->

										<!-- GALLERY TOOLS : begin -->
										<div class="gallery-tools">
											<div class="container">
												<div class="clearfix">

													<!-- GALLERY FILTER : begin -->

													<!-- GALLERY FILTER : end -->

													<!-- MORE BUTTON : begin -->
													<a href="#" class="more-btn c-button m-outline">View All Projects</a>
													<!-- MORE BUTTON : end -->

												</div>
											</div>
										</div>
										<!-- GALLERY TOOLS : end -->

									</div>

								</div>
							</section>
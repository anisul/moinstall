<?php get_header(); ?>

		<!-- BODY : begin -->
		<div id="body">

			<!-- WRAPPER : begin -->
			<div id="wrapper" class="m-large-header">

				<!-- CORE : begin -->
				<div id="core">

					<!-- PAGE CONTENT : begin -->
					<div id="page-content">
						<div class="various-content">

							<!-- FEATURES : begin -->
							<section class="c-section m-force-margin">
								<div class="section-inner">

									<div class="container">
										<div class="row">
											<div class="col-sm-3">

												<!-- ICON BLOCK : begin -->
												<div class="c-icon-block">
													<i class="fa fa-html5"></i>
													<h3>15,000+</h3>
													<p>Developers</p>
												</div>
												<!-- ICON BLOCK : end -->

											</div>
											<div class="col-sm-3">

												<!-- ICON BLOCK : begin -->
												<div class="c-icon-block">
													<i class="fa fa-apple"></i>
													<h3>7 Billion</h3>
													<p>App Requests Monthly</p>
												</div>
												<!-- ICON BLOCK : end -->

											</div>
											<div class="col-sm-3">

												<!-- ICON BLOCK : begin -->
												<div class="c-icon-block">
													<i class="fa fa-map-marker"></i>
													<h3>230</h3>
													<p>Countries</p>
												</div>
												<!-- ICON BLOCK : end -->

											</div>
											<div class="col-sm-3">

												<!-- ICON BLOCK : begin -->
												<div class="c-icon-block">
													<i class="fa fa-users"></i>
													<h3>350 Million</h3>
													<p>Unique Users</p>
												</div>
												<!-- ICON BLOCK : end -->

											</div>
										</div>
									</div>
										<hr class="c-divider m-small m-transparent">
										<p class="textalign-center"><a href="#" class="c-button m-outline m-medium">Join Now</a></p>
								</div>
							</section>
							<!-- FEATURES : end -->

							<hr class="c-divider m-negative-small m-transparent">

							<!-- GALLERY : begin -->

							<!-- GALLERY : end -->

							<!-- SERVICES SECTION : begin -->
							<section class="c-section">
								<div class="section-inner">

									<header class="section-header textalign-center">
										<div class="container">
											<h2>Monetize Smartly</h2> <br> <br>
											<p>Let top app brands know you and bid directly for your audience</p>
										</div>
									</header>

									<div class="container">
										<div class="row">
											<div class="col-md-4">

												<!-- SERVICE : begin -->
												<div class="c-service">
													<p class="service-image m-animated"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/m1.jpg" alt=""></p>
													<div class="service-ico"><i class="fa fa-rocket"></i></div>
													<div class="service-content">
														<h3>State-Of-The-Art Optimization Algorithm</h3>
													</div>
												</div>
												<!-- SERVICE : end -->

											</div>
											<div class="col-md-4">

												<!-- SERVICE : begin -->
												<div class="c-service">
													<p class="service-image m-animated"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/m2.jpg" alt=""></p>
													<div class="service-ico"><i class="fa fa-arrows-alt"></i></div>
													<div class="service-content">
														<h3>Unmatched Advertiser Diversity</h3>
													</div>
												</div>
												<!-- SERVICE : end -->

											</div>
											<div class="col-md-4">

												<!-- SERVICE : begin -->
												<div class="c-service">
													<p class="service-image m-animated"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/m3.jpg" alt=""></p>
													<div class="service-ico"><i class="fa fa-hand-o-right"></i></div>
													<div class="service-content">
														<h3>Direct Deals <br><br></h3>
													</div>
												</div>
												<!-- SERVICE : end -->

											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<p>Maximize your mobile revenue, by recommending the most relevant apps to your users, and earn more for each app installed. <br>
												Promote hundreds of global apps, and let Appnext technology deliver superior results for each ad impression.</p>
											</div>
										</div>
									</div>

								</div>
							</section>
							<!-- SERVICES SECTION : end -->

							<!-- SERVICES SECTION : begin -->
							<section class="c-section">
								<div class="section-inner">

									<div class="container">
										<div class="row">
											<div class="col-md-4">

												<!-- SERVICE : begin -->
												<div class="c-service">
													<p class="service-image m-animated"></p>
													<div class="service-ico"><i class="fa fa-terminal"></i></div>
													<div class="service-content">
														<h3>OPEN API & NATIVE ADS SDK</h3>
													</div>
												</div>
												<!-- SERVICE : end -->

											</div>
											<div class="col-md-4">

												<!-- SERVICE : begin -->
												<div class="c-service">
													<p class="service-image m-animated"></p>
													<div class="service-ico"><i class="fa fa-globe"></i></div>
													<div class="service-content">
														<h3>GLOBAL REACH <br> <br></h3>
													</div>
												</div>
												<!-- SERVICE : end -->

											</div>
											<div class="col-md-4">

												<!-- SERVICE : begin -->
												<div class="c-service">
													<p class="service-image m-animated"></p>
													<div class="service-ico"><i class="fa fa-html5"></i></div>
													<div class="service-content">
														<h3>COMMUNITY OF 15,000 DEVELOPERS</h3>
													</div>
												</div>
												<!-- SERVICE : end -->

											</div>
										</div>

									</div>

								</div>
							</section>
							<!-- SERVICES SECTION : end -->

							<!-- SERVICES SECTION : begin -->
							<section class="c-section">
								<div class="section-inner">

									<header class="section-header textalign-center">
										<div class="container">
											<h2>Advertise Differently</h2> <br> <br>
											<p>Steer your app growth, with the first Self-Serve Advertising Platform, operating on a CPI bidding basis</p>
										</div>
									</header>

									<div class="container">
										<div class="row">
											<div class="col-md-4">

												<!-- SERVICE : begin -->
												<div class="c-service">
													<p class="service-image m-animated"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/a1.png" alt=""></p>
													<div class="service-ico"><i class="fa fa-hand-o-right"></i></div>
													<div class="service-content">
														<h3>Direct CPI Bidding</h3>
													</div>
												</div>
												<!-- SERVICE : end -->

											</div>
											<div class="col-md-4">

												<!-- SERVICE : begin -->
												<div class="c-service">
													<p class="service-image m-animated"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/a2.png" alt=""></p>
													<div class="service-ico"><i class="fa fa-gear"></i></div>
													<div class="service-content">
														<h3>Transparency & Control</h3>
													</div>
												</div>
												<!-- SERVICE : end -->

											</div>
											<div class="col-md-4">

												<!-- SERVICE : begin -->
												<div class="c-service">
													<p class="service-image m-animated"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/a3.png" alt=""></p>
													<div class="service-ico"><i class="fa fa-user"></i></div>
													<div class="service-content">
														<h3>350 Million Unique Users<br><br></h3>
													</div>
												</div>
												<!-- SERVICE : end -->

											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<p>Find the right app users, on scale, tuning up your acquisition engine on your terms, and at your pace. Celebrate transparency and full control, reaching unseen performance efficiency.
												<br>
												Get featured at the top mobile apps and websites, and let high-quality global users discover and love your great apps.</p>
											</div>
										</div>
									</div>

								</div>
							</section>
							<!-- SERVICES SECTION : end -->


							<!-- PARALLAX SECTION : begin -->
							<section class="c-parallax-section m-dynamic" style="display: block; background-image: url(<?php echo esc_url( get_template_directory_uri() ); ?>/img/usrs.jpg); ">
								<div class="section-inner">

									<header class="section-header textalign-center">
										<div class="container">
											<h2>Join a Fast-Growing Community of 15,000 Developers</h2>
											<p>Dedicated To Building And Growing Great App Businesses</p>
										</div>
									</header>

									<div class="container">
										<div class="row">
											<div class="col-sm-4">

												<!-- COUNTER : begin -->
												<div class="c-counter" data-duration="1000">
													<div class="counter-icon"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/c1.png" alt=""></div>
													<h3 class="color-white">Introduce Your Apps & Get Featured</h3>
												</div>
												<!-- COUNTER : end -->

											</div>
											<div class="col-sm-4">

												<!-- COUNTER : begin -->
												<div class="c-counter" data-duration="1000">
													<div class="counter-icon"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/c2.png" alt=""></div>
													<h3 class="color-white">Get Tips and Tricks for App Growth</h3>
												</div>
												<!-- COUNTER : end -->

											</div>
											<div class="col-sm-4">

												<!-- COUNTER : begin -->
												<div class="c-counter" data-duration="1000">
													<div class="counter-icon"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/c3.png" alt=""></div>
													<h3 class="color-white">Find Business Partners</h3>
												</div>
												<!-- COUNTER : end -->

											</div>
										</div>
										<hr class="c-divider m-small m-transparent">
										<h2 class="textalign-center color-white">The Platform Developers Love</h2>
										<p class="textalign-center"><a href="#" class="c-button m-outline m-medium">Join Today</a></p>
									</div>

								</div>
							</section>
							<!-- PARALLAX SECTION : end -->

							<!-- BRANDS SECTION : begin -->
							<section class="c-section">
								<div class="section-inner">

									<header class="section-header textalign-center">
										<div class="container">
											<h2>Trusted By The Best</h2>
										</div>
									</header>

									<div class="container">
										<div class="row">
											<div class="col-sm-12">

												<!-- BRAND : begin -->
												<p class="textalign-center"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/partners.png" alt=""></p>
												<!-- BRAND : end -->

											</div>
										</div>
									</div>

									<p class="textalign-center"><a href="#" class="c-button m-outline m-medium">Join Them</a></p>

								</div>
							</section>
							<!-- BRANDS SECTION : end -->

						</div>
					</div>
					<!-- PAGE CONTENT : end -->

				</div>
				<!-- CORE : end -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>